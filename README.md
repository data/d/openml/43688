# OpenML dataset: Istanbul-Airbnb-Dataset

https://www.openml.org/d/43688

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This dataset collected from airbnb. It is collected to see how airbnb is used in Turkey Istanbul.
Content
There are 16 columns which shows the latitude, longitude etc. It also shows the price. So, a regression problem such as finding the price of an house can be applied to this dataset. To see an example you can check  my notebook from airbnb newyork dataset

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43688) of an [OpenML dataset](https://www.openml.org/d/43688). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43688/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43688/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43688/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

